import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../weather.service';
import * as moment from 'moment';

@Component({
  selector: 'app-city-results',
  templateUrl: './city-results.component.html',
  styleUrls: ['./city-results.component.scss']
})
export class CityResultsComponent implements OnInit {
  moeId: any;
  citiesData = [];
  cityData : any;
  constructor(private route: ActivatedRoute,
    private weather: WeatherService) { }

  ngOnInit() {
    this.weather.setLoading(true)
    this.moeId = this.route.snapshot.paramMap.get('woeid');
    this.weather.getWeather(this.moeId)
      .subscribe((result) => {
        this.cityData = result;
        this.cityData.applicable_date = moment(result.consolidated_weather[0].applicable_date).format("dddd MMM Do");
        result.consolidated_weather.forEach(element => {
          let city = {
            name: result.title,
            woeid: result.woeid,
            weather: element,
            date: moment(element.applicable_date).format("dddd MMM Do")
          };
          this.citiesData.push(city);
        });
        this.weather.setLoading(false)
      });
  }

}
