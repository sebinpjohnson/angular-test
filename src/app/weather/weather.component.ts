import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  @Input('city') city: any;

  constructor(private route: Router) { }

  ngOnInit() {

  }

  viewCity(woeId: number) {
    this.route.navigate(['/weather/' + woeId]);
  }
}
