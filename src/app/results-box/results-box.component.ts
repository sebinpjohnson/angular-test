import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from '../weather.service';
@Component({
  selector: 'app-results-box',
  templateUrl: './results-box.component.html',
  styleUrls: ['./results-box.component.scss']
})
export class ResultsBoxComponent implements OnInit {
  @Input('cities') citiesData: any;
  isLoading: boolean;
  constructor(private weather:WeatherService) { }

  ngOnInit() {
    this.weather.isLoading.subscribe(bool => this.isLoading = bool);
  }

}
