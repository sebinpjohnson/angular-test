import { Component, OnInit } from '@angular/core';
import { WeatherService } from './../weather.service';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  defaultCities = [2344116, 638242, 44418, 565346, 560743, 9807]; // Istanbul , Berlin , London , Helsinki , Dublin , Vancouver woeid
  citiesData = [];

  constructor(private weather: WeatherService) { }

  ngOnInit() {
    this.weather.setLoading(true);
    this.defaultCities.forEach(element => {
      this.weather.getWeather(element)
        .subscribe((result) => {
          let city = {
            name: result.title,
            woeid: result.woeid,
            weather: result.consolidated_weather[0],
            date: moment(result.consolidated_weather[0].applicable_date).format("dddd MMM Do")
          };
          this.citiesData.push(city);
          if (this.citiesData.length == this.defaultCities.length) {
            this.weather.setLoading(false);
          }
        });
    });
  }

}
