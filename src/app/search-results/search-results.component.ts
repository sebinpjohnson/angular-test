import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../weather.service';
import * as moment from 'moment';


@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {
  keyword: string;
  citiesData = [];
  showNoResult = false;
  constructor(
    private route: ActivatedRoute,
    private weather: WeatherService
  ) { }

  ngOnInit(): void {
    if (!this.keyword) {
      this.weather.setSearch(this.route.snapshot.paramMap.get('keyword'));
    }
    this.weather.currentCitiesData.subscribe(data => this.citiesData = data);
    this.weather.currentSearchString.subscribe(string => {
      this.keyword = string;
      this.weather.setLoading(true);
      this.weather.searchWeather(this.keyword)
        .subscribe((result) => {
          if (result.length == 0) {
            this.showNoResult = true
          } else {
            let cities = [];
            let allResults = result;
            result.forEach(element => {
              this.weather.getWeather(element.woeid)
                .subscribe((result) => {
                  let city = {
                    name: result.title,
                    woeid: result.woeid,
                    weather: result.consolidated_weather[0],
                    date: moment(result.consolidated_weather[0].applicable_date).format("dddd MMM Do")
                  };
                  cities.push(city);
                  if (allResults.length == cities.length) {
                    this.weather.setLoading(false);
                  }
                  this.weather.setCitiesData(cities);
                });
            });
          }
        });
    });
  }

}
