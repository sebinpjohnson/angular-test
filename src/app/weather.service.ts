import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const mainRoute = `api/weather.php`

const routes = {
  search: (keyword: string) => mainRoute + `?command=search&keyword=${keyword}`,
  getWeather: (woeId: number) => mainRoute + `?command=location&woeid=${woeId}`
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private httpClient: HttpClient) { }

  private searchString = new BehaviorSubject('');
  private citiesData = new BehaviorSubject([]);
  private loading = new BehaviorSubject(false);

  currentSearchString = this.searchString.asObservable();
  currentCitiesData = this.citiesData.asObservable();
  isLoading = this.loading.asObservable();

  setLoading(bool: boolean) {
    this.loading.next(bool);
  }
  setSearch (string: string) {
    this.searchString.next(string);
  }

  setCitiesData(data: any) {
    this.citiesData.next(data)
  }
  getWeather(woeId: number): Observable<any> {
    return this.httpClient.get(routes.getWeather(woeId))
      .pipe(
        map((body: any) => body)
      )
  }

  searchWeather(keyword: string): Observable<any>{
    return this.httpClient.get(routes.search(keyword))
    .pipe(
      map((body: any) => body)
    )
  }
}
