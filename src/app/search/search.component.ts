import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchString: string;
  constructor(private route: Router,private weather: WeatherService) { }

  ngOnInit() {
  }

  search() {
    this.weather.setSearch(this.searchString);
    this.route.navigate(['/search/' + this.searchString]);
  }

  goHome() {
    this.route.navigate(['/']);
  }

}
