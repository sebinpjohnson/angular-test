import { Component } from '@angular/core';
import { WeatherService } from './weather.service';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private weather: WeatherService) { }

  ngOnInit() {
    
  }
}
